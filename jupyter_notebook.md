# Jupyter Notebook

*Objectives*
1. [Setup a Jupyter Notebook](#starting-a-jupyter-notebook)
1. [Add conda kernels (optional)](#add-conda-kernels)

## Prerequisites
1. [a VM on GCP](./vm.md) with:
    - a [firewall rule](./vm.md#define-a-firewall-rule) for port 8888
    - [Anaconda](./vm.md#install-anaconda)
    - [Git](./vm.md#install-git)

## Starting a Jupyter Notebook
The Jupyter Notebook is an open-source web application that allows you to create and share documents that contain live code, equations, visualizations and narrative text.

1. In the SSH browser window, start a Jupyter Notebook: `jupyter notebook --ip=0.0.0.0 --port=8888 --no-browser`
    <center><img src="resources/img/jupyter-notebook-server.png" width="500"></center>
1. To open in a web browser, navigate to the VM's `<external IP>:8888`.
    <center><img src="resources/img/jupyter-notebook-external-ip.png" width="500"></center>
    <center><img src="resources/img/jupyter-notebook-token.png" width="500"></center>

    - It will prompt for a password or token which can be found in the SSH browser window where the notebook server is running.
1. You should now be able to navigate the file tree where the Jupyter Notebook server was started (ie where you entered the `jupyter notebook` command).
    <center><img src="resources/img/jupyter-notebook.png" width="500"></center>

To exit the server in the SSH browser window, use `CTRL-C`.

## Add conda kernels
To use a predefined conda kernel in Jupyter Notebooks, run the command `conda install nb_conda_kernels`. This will add all environments/kernels automatically.

Reference:
 - [GitHub: nb_conda_kernels](https://github.com/Anaconda-Platform/nb_conda_kernels)
