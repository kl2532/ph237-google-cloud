# Software Tools

## Linux
Linux is a family of free and open-source software operating systems (OS) built around the Linux kernel, which is the layer between the hardware and software. Linux OS is commonly used in data centers with Microsoft's Windows OS as the other variant. It's portability among various hardware architectures makes it a popular OS.

Some popular Linux flavors include:
- Arch Linux
- CentOS
- Debian
- Ubuntu

These distributions differ in the default tools included, user interface, and much more.

We will be using the `bash` shell, which provides a command line user interface to interact with the operating system. `bash` can be found within a terminal emulator such as Terminal (MacOS) or [cmder](http://cmder.net/) for Windows.

### Commands
Here is a sampling of a few useful commands:
- `pwd`: present working directory
- `cd`: change directory
- `ls`: list directory
    - to view hidden directories and files (ie those that begin with `.`), use `ls -a`.
- `mkdir`: make directory
- `rm`: remove file
- `mv`: move file
- `cat`: output contents of a file
- `cp`: copy file

There are hundreds of commands and difficult to know them all at the start. Always search first to see if there's already a command for the task you're trying to accomplish before hacking together your own solution. You'll build up your knowledge of Linux commands!

To learn more about the command, read through the manual page with the command `man <NAME>` that is a tool which formats and displays the on-line manual page.

For example, to learn more about the `cat` command, we would use `man cat`:
<img src="resources/img/man-cloud-shell-cat.png"  width="500">

which will open the man pages as such:

<img src="resources/img/man-linux-cat.png"  width="500">

To exit the man pages, press `q`.

### Text Editor
Occasionally, you will need to interface with standard
Linux text editors such as `vim`, `emacs`, or `nano`. This allows us to remain in the terminal environment rather than opening a separate text editor application such as TextEdit and Notepad. Most of the time, graphical user interfaces (GUIs) are not available to open up separate applications.

This is a contentious debate among programmers as to which text editor is better -- `vim` or `emacs`. However, we will avoid this by using `nano` which is much friendlier as it lists basic commands at the bottom.

<img src="resources/img/nano-linux-hello.png"  width="500">

<img src="resources/img/nano-cloud-shell.png"  width="500">

We can exit nano by `CTRL+X`. It will ask if you wish to save the file:

<img src="resources/img/nano-save.png"  width="500">

Press 'Y' to save. Then, it will ask to confirm the filename:

<img src="resources/img/nano-filename.png"  width="500">

Press `enter` to keep the existing filename.

## Version Control
You may have your own way of keeping track of file versions such as adding a timestamp or
version number to a filename. However, this method is error prone since it's easy
to forget which directory you're working in and accidentally overwrite the wrong file. Version control keeps track of file changes for you.

The old way of collaboration was emailing file changes back and forth or asking your coauthor to not edit a file if it's on a centralized filesystem. Version control promotes more fluidity through automatic file merges.

We will be using `git`. To learn more about git, read our dedicated [git](./git.md) page.

Note that Git is separate from the previous Linux commands mentioned. As an example, removing (`rm`) a file from the filesystem will not be visible from the perspective of Git. This is for privacy and to separate Git from the existing filesystem.

## Reproducibility
Version control helps keep track of files but we also leverage existing libraries such as pandas or data.table. When a library gets updated (due to bug fixes, feature implementation), the version number also gets updated. However, some libraries depend on older versions and updating the library may cause code to break.

We need a way to keep track of which library version we're working on through the use of a package manager and virtual environments. Virtual environments isolate package installation from the system.

We can check where Python/R looks for packages and how a conda environment will change the path:
- Python: `python -c 'import sys; print(sys.path)'`
    - Default Python path:
    <img src="resources/img/anaconda-python-before.png"  width="500">

    - Anaconda Python path:
    <img src="resources/img/anaconda-python-after.png"  width="500">
- R: `.libPaths()`
    - Default R path:
    <img src="resources/img/anaconda-R-before.png"  width="500">

    - After creating a conda environment (`conda create -n r r-essentials`) and activating it (`conda activate r`):
    <img src="resources/img/anaconda-R-after.png"  width="500">


As seen above, all the libraries would be saved to the same folder with the system defaults. Since we can only have 1 version of 1 library, this will cause issues. Using a conda environment creates a separate folder that will house all of the library downloads once the environment is activated.

We primarily use Anaconda since it can be used for both Python and R. However, we will briefly mention popular package managers used in each language. In addition, Docker is a popular containerization tool that is relatively new.

To show the various package managers, we will use TensorFlow, an open source machine learning framework, as a concrete example.

### Anaconda
Anaconda is an open-source package and environment manager. It is platform-agnostic, so it can be used on Windows, macOS, or Linux. We commonly write code for analysis on a specific dataset, leave the code untouched for a few months, and revisit it once a new dataset arrives. Then, when we try to use our old work, we experience a bunch of error messages since we've been working on other projects and have selectively updated packages. One possible solution is to update all of the packages. But, this may fail if one package relies on an older version of another package since the package maintainer may not have had a chance to update the package to work with the updated dependency.

To prevent this "dependency hell", we can create a conda environment that can be shared with others to promote reproducibility. One benefit of Anaconda is that sudo/admin privileges are not required. While this is not the case for our VM, no sudo/admin privilege is often the situation for a shared cluster.


For example, to [install TensorFlow from conda-forge](https://anaconda.org/conda-forge/tensorflow), we would use the following command:

```
conda install -c conda-forge tensorflow
```

The `-c` flag specifics the specific channel to download from. The [conda-forge](https://anaconda.org/conda-forge) channel is a community driven collection of packages for conda that can easily be installed using `conda install`.

To learn more about installing Anaconda on a VM: [VM: Install Anaconda](./vm.md#install-anaconda.md)

### Python
Python packages are typically installed from one of the two package repositories:
1. [PyPI: Python Package Index](https://pypi.org/)
1. [Conda](https://anaconda.org/conda-forge)

**pip** package manager is a command line program that is frequently used to install from PyPI, version control, local projects, and directly from distribution files.

For example, TensorFlow provides documentation on [installing with pip](https://www.tensorflow.org/install/pip). The command to install the TensorFlow pip package is:

```
pip install --upgrade tensorflow
```

It is recommended to create a virtual environment using `virtualenv` and activating (changing where the system looks for packages) prior to installing with the command:

```
virtualenv --system-site-packages -p python3 ./venv
```

### R
[Packrat](https://rstudio.github.io/packrat/) is a dependency management system for R. It gives each project its own package library, can be ported from one computer to another, and promotes reproducibility by recording the exact package version.

Once Packrat is initialized, we can install TensorFlow using instructions from RStudio: [Installing TensorFlow](https://tensorflow.rstudio.com/tensorflow/articles/installation.html). Note that this is a wrapper for the Python TensorFlow.

### Docker
Docker is an open-source platform for operating-system level virtualization
("containerization") and offers a centralized image management server.
Containers are not new! Some examples include Solaris Zones, FreeBSD jails,
and Linux LXC. However, Docker nicely packaged the containerization experience
through their `docker` commands and provides an ecosystem to easily store
public images.

>Terminology: Container vs Image
>    - `Container`: a container is a running instance of an image. (analogy: a process running the program source code)
>    - `Image`: a set of layers that describe the container to build. (analogy: program source code)

<center><img src="resources/img/vm_diagram-VM-vs-Docker.png" width="600"></center>

Docker containers are lightweight and faster compared to VMs that need to boot
up an entire virtual OS every time. For environment replication, it's much
easier to share a Docker image by providing the image name on Docker Hub than
passing around a VM. This is commonly used for production systems and requires `sudo` (admin access).

Take a look at [TensorFlow Docker](https://www.tensorflow.org/install/docker) to see what steps are required for a TensorFlow-configured container.


## Development Environment
Having a coding environment that you are comfortable with can greatly increase
your productivity. Some enjoy the simplicity of text editors while others
prefer an integrated development environment (IDE) that provides development
tools such as packaging, git, debugging all in one place.

Here are a few tools that we've explored. This is by no means extensive as
new tools are constantly being developed.

### Atom
A hackable text editor for the 21st Century.  
https://atom.io/

### Vim
Vim is a highly configurable text editor for efficiently creating and changing
any kind of text.  
https://www.vim.org/

### Rstudio
Open source and enterprise-ready professional software for R.  
https://www.rstudio.com/

### Jupyter Notebooks
The Jupyter Notebook is an open-source web application that allows you to
create and share documents that contain live code, equations, visualizations
and narrative text.  
http://jupyter.org/
