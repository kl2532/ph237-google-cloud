# GCP Setup

*Objectives*
1. [Set up billing account using a coupon code](#billing)
1. [Create a new GCP project and link billing account](#create-a-new-project)

All GCP services are associated with a project. Each project is used to:
- track resource and quota usage
- enable billing
- manage permissions and credentials
- enable services and APIs

## Billing
1. Generate your [coupon code](https://google.secure.force.com/GCPEDU/?cid=RiIwuUIH7apHVQAYaBhElzJEF7xC7BCqPsIxDIOij78JAjMHR6SpSTUukIc4axd3/) using your `@berkeley.edu` email
1. With your coupon code, redeem your Google Cloud credits via
https://console.cloud.google.com/education
1. Once the coupon is applied, you should arrive at the "Billing" page with
credits available!

We now have a billing account to charge to but we'll need to create a project before
exploring what Google Cloud has to offer. By organizing into projects/resources,
we can easily migrate the project to another organization, delete the project if we
are finished (and save $$), or change the project to a different billing
account.

## Create a new project
1. To create a new project, open the navigation panel and go to IAM & admin >  Manage resources.
    <img src="resources/img/manage_resources.png"  height="500">
1. Select "Create Project" at the top menu bar.
    <img src="resources/img/create_project.png" width="500">
1. Enter a Project Name and select the correct billing account
    - In the screenshot:
        - Project Name: ph237-test
        - Billing account: Health Seminar: Data Science in Health Policy (PH293.X)
    <img src="resources/img/new_project.png" width="500">
