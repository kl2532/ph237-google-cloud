# Collaboration on GCP

A researcher's workflow is often:
1. Ingestion: Receive a dataset
1. Staging: Cleaning up the data, exploratory analysis
1. Computation: Modeling, sharing results
1. Finalize analysis, write a paper

Most of the analysis is currently done locally and results shared in various email threads. We can imagine doing everything on a centralized platform that can accommodate collaborators of different backgrounds.

## On GCP

**Ingestion**  
Your collaborator uploads a dataset to Cloud Storage using `gsutil cp shared-data-file.txt gs://your-bucket-name-here`.
- `gsutil`: Google Cloud Storage utilities command line interface
- `cp`: copy
- `shared-data-file.txt`: source file to copy
- `gs://your-bucket-name-here`: destination of uploaded file

If you are copying multiple files, you can add the `-m` flag to run this in parallel such as `gsutil -m cp *.txt gs://your-bucket-name-here`.

On your GCP VM, download all data files from Cloud Storage using `gsutil -m cp -r gs://your-bucket-name-here .` The `-r` flag recursively copies the contents of the bucket. The data will now be downloaded to your VM. We can confirm this by the `ls` command.

As an example, we have uploaded the CSV files from the [SyntheticMass dataset](https://syntheticmass.mitre.org/download.html). We can download the files using `gsutil -m cp -r gs://ph237-data-challenge .`

<center><img src="resources/img/collaboration-gcp.png" width="600"></center>

To learn more about the `gsutil cp` command: https://cloud.google.com/storage/docs/gsutil/commands/cp

**Staging / Computation**  
The data will now be ready for analysis. We can clean the data, push the data to GitLab/GitHub/Bitbucket, and share initial exploratory analysis in a Markdown file for others to see.

**Paper**  
When the team finalizes the analysis, all of the data, code, and analysis is in one place. This makes it easy to repeat analysis (if needed) and confirm that all team members are on the same page.

## Local Computer

If GCP is used primarily for the purposes of a centralized data sharing platform, we will download the data onto our local computer or compute cluster.

We would first need to download the `gsutil` command line tool from the [Google Cloud SDK](https://cloud.google.com/sdk/docs/), install it, and initalize the SDK. Then, we can run the same commands as above to download the data:
- upload data: `gsutil cp shared-data-file.txt gs://your-bucket-name-here`
- download data: `gsutil -m cp gs://your-bucket-name-here .`
