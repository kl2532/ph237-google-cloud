# Git

https://xkcd.com/1597/

Git is a popular open-source distributed version control system that keeps
track of changes to files. This provides a means to review changes over time,
revert back to a previous state, simultaneously collaborate with others, and
much more! Your changes are tracked locally through a few commands (git add,
git commit) and can be shared with others through a central hosting
service either in the cloud or on-premise.

There are several web-based hosting services for Git such as:
- [GitHub](https://github.com/)
- [GitLab](https://gitlab.com/)
- [Bitbucket](https://bitbucket.org/)

They provide additional features such as a place to log issues, document
the project in the wiki, and an organized manner of collaboration through
pull/merge requests (PR, MR).

## Git Basics
Git differentiates itself from other version control systems in how it stores and thinks about information such as:
- data as a series of snapshots of a miniature filesystem
- nearly every operation is local (reduce network latency)
- integrity with check-sums
- generally only adding data

Git stores data in a **stream of snapshots**, not differences. Other version control systems such as Subversion store files and the changes made to each file over time (a delta-based version control). Each `git commit`, the command to save the state of the project, takes a snapshot of the files and stores a reference to that snapshot. If the file hasn't changed, Git will link to the previous identical file it has already stored. This makes Git more like a mini filesystem.

With a Git version controlled project, you have a **local database** of the project. For example, browsing the history of the project can be done without a network connection since it simply reads directly from your local database. This means that you can still do work on your files when you're offline or off VPN.

To preserve **file integrity**, Git uses a SHA-1 hash such as the following:

```
b77dc3da2d9716cb85e8c98bbdbe219b5ed61e85
```

It's a 40 character string and calculated based on the contents of a file or directory structure. These hash values are used in the Git database.

Finally, with Git, most actions **add data** to the Git database. This helps with experimentation since we know that we can retrieve an old file as long as it has been committed into Git.


## Getting Started

### Set up Git
[Download Git](https://git-scm.com/download/) or check if you already have it installed:

<img src="resources/img/git-version.png" width="500">

Set your user name and email address that will be used for every Git commit. These are global configurations to be done per system.
```
git config --global user.name "First Last"
git config --global user.email email@email.com
```

### Create an account on GitHub/GitLab/Bitbucket
Create an account on a version control repository hosting service:
- [GitHub](https://github.com/)
    - [GitHub Education](https://education.github.com/)
    - [GitHub Student Developer Pack](https://education.github.com/pack): free access to several developer tools including unlimited private repositories
- [GitLab](https://gitlab.com/)
    - public projects on GitLab.com with all functionality and private projects with the free functionality
    - [GitLab for Education](https://about.gitlab.com/education/)
- [Bitbucket](https://bitbucket.org/)
    - [Bitbucket Education](https://bitbucket.org/product/education): free unlimited private repositories

Each hosting service provides slightly different features and user interface. Explore them all to determine which suits you best!


### Setting up an SSH key
This is an optional step that allows you to authenticate to a remote server without typing your username and password each time you `git push` your changes for others to see on the remote server. The secure shell (SSH) protocol establishes a cryptographically secured connection between you and a remote server by authenticating each side to the other.

You will need the OpenSSH client installed, which is pre-installed on GNU/Linux and macOS. The 2 common authentication key types are: RSA and ED25519. They differ in their signing algorithm which exploit different mathematical properties. ED2219 SSH keys, introduced in OpenSSH6.5, are more secure and have better performance so we will use this over RSA.

#### Generating a new SSH key pair
1. In terminal, generate a new ED25519 SSH key pair (public/private):
`ssh-keygen -t ed25519 -C "email@example.com"`. If RSA is preferred: ` ssh-keygen -o -t rsa -b 4096 -C "email@example.com"`
    - the `-C` flag adds a comment to the key to distinguish multiple keys.
1. It will prompt you to input a file path to save the SSH key pair to. Press 'Enter' to use the suggested path if you don't already have an SSH key pair.
    - If you already have an SSH key pair with the suggested file path, you will need to input a new file path and set up your configuration file.
1. Next, it will prompt for a passphrase to secure the SSH key pair. It's best practice to enter a passphrase, but it's not required (press 'Enter' twice).

<img src="resources/img/git-ssh-key.png" width="500">

#### Adding an SSH key to your account
1. Copy the **public** SSH key to your clipboard.
    - Use your newly learned Linux skills to open the file in `nano` and copy the contents.
1. Add your public SSH key to your GitLab account by clicking your avatar in the upper right, and selecting 'Settings'. Navigate to **SSH Keys** and paste your public key in the "Key" section. Make sure you use your public key -- the one with '.pub' and not your private key. If you created a key with a comment, this will appear under "Title". If not, add an identifiable title. Finish adding the key by clicking **Add key**.
    - This step will differ for each hosting service but the general workflow will be similar.

Reference:
- [GitLab Docs: SSH](https://docs.gitlab.com/ee/ssh/#working-with-non-default-ssh-key-pair-paths)

## Initial Project
There are two ways to start an initial project:
1. When beginning a new project, create a completely new Git repository using `git init`.
1. When working off of an existing project, clone the repository using `git clone`.

Both of these create the hidden subdirectory `.git` on your local machine which houses the entire repository -- the metadata and object database.

### Creating a new repository
To put a directory under version control, go to the project directory and type `git init`.

This creates a subdirectory `.git` that contains a Git repository skeleton.

<img src="resources/img/git-init.png" width="500">

Next, we will create a new project on a version control hosting service. Note that the following screenshots are from GitLab but all of commands should be similar.

<img src="resources/img/git-gitlab-new-project.png" height="500">

Once a new project is created, we need to link our local Git repository to the server. Towards the bottom of the page are some helpful command line instructions:

<img src="resources/img/git-gitlab-command-line-instructions.png" height="500">

- *Git global setup*: We've already done this in [Getting Started](#getting-started).
- *Create a new repository*: Because we used `git init`, we have an existing repository set up.
- *Existing folder*: This is the section that will be most relevant for us since we want to add a remote, which is a set of managed repositories.
- *Existing Git repository*: This would be used in the case where you have an existing remote called 'origin' on GitHub, for example, and wish to add a remote to GitLab.

We would use the following command from the above screenshot:
`git remote add origin git@gitlab.com:kl2532/first_git_project.git`.
- `git remote`: command to manage set of tracked repositories
- `add <shortname>`: adds a remote with the shortname 'origin'
- `<url>`: location of the remote

We can check that this remote called 'origin' has been added to the set of managed repositories by the command `git remote -v`.

<img src="resources/img/git-gitlab-remote.png" width="700">

Now we're all set up to track files, commit to the local repository, and push to the remote repository.

### Cloning an existing repository
If you want a copy of an existing Git repository, type `git clone <url>`. This will download every version of every file for the history of the project.

The following example clones the open source project [TensorFlow](https://github.com/tensorflow/tensorflow) via HTTPS.
<img src="resources/img/git-clone-tensorflow.png" width="700">

If we wished to add a different remote such as our own personal GitLab project, we would follow the command line instructions for 'Existing Git repository', specifically the command `git remote rename origin old-origin`:
- `git remote`: command to manage set of tracked repositories
- `rename origin old-origin`: rename the remote named <old> ('origin') to <new> ('old-origin')

## Git Workflow

You are writing code for analysis on a dataset and have reached a point where the code produces valuable findings. You want to save a version of this code before making any additional modifications in case the team wants to revisit this analysis. You would `git add` all relevant files from the working directory to the staging area. Then `git commit` all of the staged files to the local repository with a helpful message so others know the purpose of this commit. Finally, `git push` your local changes to the remote repository so the rest of the team can see your changes.

```mermaid
  graph LR
    step1(Working Directory) -- git add --> step2(Staging Area)
    step2 -- git commit --> step3(Local Repository)
    step3 -- git pull/push --> step4(Remote Repository)
```

### Working Directory
Each file in your working directory can be in two states: *tracked* or *untracked*.
Tracked files are the only files that Git is aware of. Untracked files usually contain sensitive information. If you clone a Git repository, all of the files are tracked and unmodified because Git just checked them out and you haven't modified anything.

```mermaid
sequenceDiagram
    participant Unmodified
    participant Modified
    participant Staged
    Unmodified->>Modified: edit the file
    Modified->>Staged: stage the file
    Staged->>Unmodified: commit
```

`git status` shows us the state of the repository from the perspective of Git.

<img src="resources/img/git-git-status-initial.png" width="700">

We can create an empty file using the `touch` command. Now, when we `git status`, we see that the newly created file is untracked. This is to prevent Git from tracking binary files or sensitive files automatically.

<img src="resources/img/git-git-status-touch.png" width="700">

To begin tracking a new file, we use the command `git add`.

<img src="resources/img/git-git-add.png" width="700">

Let's add another file called 'sensitive.txt'. We can see that 'sensitive.txt' is under the 'Untracked files' header and 'README.md' is under the 'Changes to be committed' header.

<img src="resources/img/git-git-status-sensitive.png" width="700">

We can create a '.gitignore' file for files or folders that we don't want to track. Although we only have one file we don't want to track, this can be extremely helpful in the scenario where we don't want an entire folder to be tracked and cluttering up the output of `git status`. Github maintains a list of [.gitignore files](https://github.com/github/gitignore) as a good starting point.

We see that 'sensitive.txt' is no longer listed as an untracked file but '.gitignore' is now an untracked file as we would expect.

<img src="resources/img/git-gitignore.png" width="700">

### Staging Area - Add
The staging area is where the files that have been `git add` are located. In the output of `git status`, this is designated with the header 'Changes to be committed'. This selective staging area allows you to add files that you are ready to commit to the local repository.

For example, if you are working on a few files but are not ready to commit to the local repository, the files would remain as tracked but not staged. This allows you to add files that others are depending on but not worry about including files you are still working on.

Another way to think about the staging area is as a place to mark files to go into the next commit snapshot.

Let's add '.gitignore' to be tracked by Git. Now we have two files in the staging area.

<img src="resources/img/git-gitignore-add.png" width="700">


### Local Repository - Commit
To formally commit these to the local repository, we use the command `git commit -m` with the `-m` flag to include a log message to describe the changes made. This log message should be succinct and can be used for other users to understand a high level summary of what changes were made in this commit.

<img src="resources/img/git-git-commit.png" width="700">

Now we've made our first commit! The files are safely stored in your local database on the default 'master' branch with a SHA-1 checksum (e3ba90b in above screenshot). To summarize, `git commit` takes the current contents of the staging area and stores that snapshot permanently to your Git directory.

**Undoing a previous commit**  
If you commit too early and forget to add some files or mess up the commit message, you can redo that commit using the `--amend` option.

```
$ git commit -m 'initial commit'
$ git add additional_file
$ git commit --amend
```

This uses the second commit to replace the first commit and helps declutter your repository history.

### Remote Repository - Push
Finally, we push all of our changes to the remote repository using `git push <remote> <branch>`. Recall that we defined the remote as 'origin' and the default branch is 'master'. If you are collaborating with others, remember to `git pull <remote> <branch>` to retrieve others' work prior to pushing your own.

<img src="resources/img/git-git-push.png" width="700">

### Other Tips
**Removing Files**  
Removing a file from Git requires the command `git rm` to remove it from the staging area and commit. Using the standard `rm` command removes the file from the filesystem but does not remove it from the Git repository.

**Moving Files**  
Git doesn't track file movement either. To rename a file, the command is `git mv`. This is equivalent to

```
$ mv README.md README
$ git rm README.md
$ git add README
```

## Collaboration

When others are contributing to the same project, you will need to define a standard protocol. Will you all be working off of the master branch? Or will each person have their own branch that will be merged? Each team's workflow is slightly different so this should be discussed amongst all members.

`git pull` is the command to use to fetch data from the server and automatically merge it with your code. It is the combination of two commands:
- `git fetch <remote>`: goes to that remote project and downloads all data from the remote project but does not modify your working directory
- `git merge <branch>`: joins two or more development histories together

Other people working on the same project may have different ideas of how something should work. This may lead to merge conflicts which need to be resolved manually since Git does not know which idea is better. The person resolving the merge conflict will need to review both ideas and determine which one is preferred. The branching feature is the main advantage of Git -- it allows everyone to be working on a branch of the master you've all checked out. 

We will walk through an example of how the following graph was created:
`git log --oneline --graph --decorate --all`

<img src="resources/img/git-merge-branch.png" width="700">

Commit `5a9819b` is referred to as a *merge commit* since it has more than one parent.

### Merge Conflicts
When multiple people are working on the same project, it is important to frequently sync the project to retrieve changes by using the `git pull` command. Git is usually good at resolving merges but occasionally you will need to manually resolve the merge.

We will simulate a merge conflict scenario. Let's populate our README.md with some text, commit it, and push it to the GitLab project. I'll be using the text from [Veggie ipsum](http://veggieipsum.com/) but you are welcome to use something else. *Note:* We are populating the README.md with nonsense only for the purposes of simulating a merge conflict. Please include useful documentation in the README.md for your projects!

<img src="resources/img/git-readme-veggie-ipsum.png" width="700">

A reminder of how to add, commit, and push:

<img src="resources/img/git-readme-update.png" width="700">

Now, navigate to your project on GitLab and go to the README.md file.

<img src="resources/img/git-gitlab-readme.png" width="500">

We will edit the file using the Web IDE.

<img src="resources/img/git-gitlab-readme-webide.png" width="500">

To simulate a merge conflict, we'll need to insert text in one of the paragraphs. I'll be inserting some [Bacon ipsum](https://baconipsum.com/?paras=5&type=all-meat&start-with-lorem=1) in the middle of the second paragraph.

<img src="resources/img/git-gitlab-readme-bacon.png" width="500">

We will commit this using the Git GUI on the left.

<img src="resources/img/git-gitlab-readme-bacon-commit.png" width="500">

If we navigate back to the project, we see that the changes have populated successfully!

Going back to our local repository, we will edit that same paragraph.

<img src="resources/img/git-readme-hello-world.png" width="500">

Add the file to the staging area, commit, and push to the remote repository. From the hints accompanying the error messages, we see that we forgot to pull before pushing!

<img src="resources/img/git-merge-conflict-push.png" width="700">

Let's pull the changes we made in GitLab. Usually Git will resolve merges on its own if additional lines are added but sometimes there are conflicts where Git does not know which version to use.

<img src="resources/img/git-merge-conflict.png" width="700">

If we open the file, we will see some odd annotations:

<img src="resources/img/git-merge-conflict-contents.png" width="500">

There are 2 sections: the contents from our file (denoted by `<<<<<<< HEAD`) and the contents from the conflicting file (denoted by `>>>>>>> 50a9b8`) with a separator `=======`. We can manually delete the lines to remove.

I'll keep the version with 'Hello world!' but you are welcome to select either version.

<img src="resources/img/git-merge-conflict-fix.png" width="500">

Now we're ready to add, commit, pull, and push!

<img src="resources/img/git-merge-conflict-fixed-push.png" width="700">

We've successfully resolved a merge conflict! While we only had 1 merge conflict since we only changed 1 paragraph, sometimes there will be multiple merge conflicts. Be sure to resolve all of the merge conflicts before pushing back to the remote repository. Your teammates will appreciate not having to decipher which version is correct.

## Documentation
Functioning code is always a priority but a well documented project takes it a step further. It encourages
others to review your project and perhaps even contribute! More importantly, it helps you recall what you did
and why you did it.

Documentation can occur within the code in the form of comments and docstrings. Or it can be at a higher level
such as a Markdown (.md) document or wiki. However, the best form of documentation is one that people will
actually use!

Personally, I use Markdown documents for documentation because they can be easily tracked with git and are tightly
coupled with the code without needing to read through a bunch of code to understand the high level function.
I view wikis as a place to put final documentation.

One thing to note is that each hosting service has a different flavor of Markdown. For example, GitLab Markdown
rendering includes the Mermaid javascript library for flow charts such as the one seen above.

To read more about each flavor of Markdown:
- [GitHub Flavored Markdown Spec](https://github.github.com/gfm/)
- [GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html)
- [Bitbucket Markdown](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

## Summary of Git Commands
- `git init`: initialize a project / repository
- `git clone`: clone a repository into a new directory
- `git add`: move a file from working directory to staging area
- `git commit`: take a snapshot of file changes
- `git pull`: fetch and integrate with another repository or a local branch
- `git push`: push changes to remote repository

## Helpful Troubleshooting
`git status`: shows the status of the project from the perspective of Git

`git diff`: check difference between what's in the staging area and what's in the working directory  
- press 'q' to exit diff mode

`git log`: a list of commits
- 40 character code (SHA-1 hash) that uniquely identifies the commit
- author of the commit
- date and time of commit

`git help <verb>` or `man git-<verb>`: access the manual pages for any Git command

## References
- [Git](https://git-scm.com/)
- [GitLab Flow](https://gitlab.com/help/workflow/gitlab_flow.md)
- [GitHub Cheatsheet](https://services.github.com/on-demand/downloads/github-git-cheat-sheet/)
- [Happy Git and GitHub for the useR](http://happygitwithr.com/)
