# Virtual Machines

*Objectives*
1. [Understand what a virtual machine is](#what-is-a-vm)
1. [Create a VM](#create-a-vm)
1. [Install Git](#install-git)
1. [Install Anaconda](#install-anaconda)
1. [Define a Firewall Rule to expose ports](#define-a-firewall-rule)


## What is a VM

A virtual machine (VM) is an emulation of a computer system. It runs on top of
a physical machine (like Google's servers) with the help of a "hypervisor".
The hypervisor (or virtual machine monitor) provides VMs with virtualized
hardware such as RAM and CPU that is defined by you!

<center><img src="resources/img/vm_diagram.png" width="500"></center>

Our laptop is a personal computer designed for individual use in terms of size,
functionality, and price. In the cloud, we will create a VM on a
server that will act as our personal computer -- it's like getting a new
computer without the high price tag!

## Create a VM

1. To create a new VM instance, navigate to "Compute Engine" > "VM instances" and select "Create Instance".
    <center><img src="resources/img/vm_instances.png" width="500"></center>
1. Name your VM.
1. Select '1 vCPU'.
1. Select 'Ubuntu 16.04 LTS' as the OS image to boot from.
1. Check 'Allow HTTP traffic'. This opens the firewall 80 (HTTP) for network traffic communication.

<center><img src="resources/img/vm-create-vm-instance.png" height="500"></center>

The [Region and Zone](https://cloud.google.com/compute/docs/regions-zones/)
fields should be automatically populated. This can be customized for
computational needs such as GPU availability or specific Google product/service
requirements (some beta services may only be available in a certain
regions/zones).

## Install Git
To install git on Ubuntu: `sudo apt-get install git`.

To check that it installed properly: `git --version`.

<img src="resources/img/vim-git-version.png" width="400">

## Install Anaconda
Anaconda is an open-source package and environment manager. It is platform-agnostic, so it can be used on Windows, macOS, or Linux.

Package updates occur quite frequently which is good for bug resolutions and
new features but can be the cause of frustration when trying to run someone
else's code. When working on a local machine, packages are usually installed globally.
This may cause conflicts for projects that depend on different versions.

To prevent this "dependency hell", we will create a conda environment that can be shared with others to promote reproducibility. One benefit of Anaconda is that sudo/admin privileges are not required. While this is not the case for our VM, no sudo/admin privilege is often the situation for a shared cluster.

1. Connect to the VM via `SSH`. Secure Shell (SSH) is a program for logging
into a remote machine. It provides secure encrypted communications between
two untrusted hosts over an insecure network. There are various methods to
connect to the VM as listed in the dropdown. We will be using the default
option: "Open in browser window".
    <center><img src="resources/img/vm-ssh-browser.png" width="500"></center>
1. It's good practice to periodically update packages. To do that, type
`sudo apt-get update` into the terminal.
    <center><img src="resources/img/vm-sudo-apt-get.png" width="400"></center>

    - `sudo`: execute command as a superuser (root) for additional privileges
    - `apt-get`: tool for handling packages using the APT library
    - `update`: resynchronize the package index files from their sources
1. Next, we will install Anaconda: `wget <anaconda_download_link>`
    - To find the `anaconda_download_link`, navigate to the [Anaconda Download](https://www.anaconda.com/download/#linux) page and copy the link for the Linux Installer for Python 3.7 version.
    - As an example: `wget https://repo.anaconda.com/archive/Anaconda3-5.3.0-Linux-x86_64.sh`. However, software versions update quite frequently so please use the most up to date version at [Anaconda Download](https://www.anaconda.com/download/#linux).
    <center><img src="resources/img/vm-download-anaconda.png" width="500"></center>
1. Make the downloaded file an executable by `chmod +x <Anaconda_version.sh>`
    - `chmod`: change file mode bits
    - As an example: `chmod +x Anaconda3-5.3.0-Linux-x86_64.sh`
    <center><img src="resources/img/vm-anaconda-executable.png" width="500"></center>
1. Anaconda depends on the system tool `bzip2`. We will install this with the command `sudo apt-get install bzip2`.
    <center><img src="resources/img/vm-install-bzip2.png" width="500"></center>
1. Run the executable: `./Anaconda3-5.3.0-Linux-x86_64.sh`. This will take several minutes to install.
    - Press `q` when the "Anaconda End User License Agreement" appears. Then, type `yes` to accept the license terms.
    <center><img src="resources/img/vm-anaconda-install-license-terms.png" width="500"></center>

    - Press `ENTER` to confirm installation location.
    <center><img src="resources/img/vm-anaconda-install-location.png" width="500"></center>

    - Type `yes` when asked to initialize Anaconda in your .bashrc.
    <center><img src="resources/img/vm-anaconda-install-bashrc.png" width="500"></center>

    - Type `no` when asked to install Microsoft VSCode which is an integrated development environment.
    <center><img src="resources/img/vm-anaconda-install-vscode.png" width="500"></center>

1. Once you are finished installing, run `source .bashrc` to read and execute the newly added information from the Anaconda installer.
    - To test if the installation was successful, check the conda version: `conda --version`
    <center><img src="resources/img/vm-anaconda-check-installation.png" width="400"></center>

### Conda environment
Package updates occur quite frequently which is good for bug resolutions and
new features but can be the cause of frustration when trying to run someone
else's code. When working on a local machine, packages are usually installed globally.
This may cause conflicts for projects that depend on different versions.

To prevent this "dependency hell", conda environments separate packages. Read the [conda docs](https://conda.io/docs/user-guide/tasks/manage-environments.html) to learn more about managing environments.

## Defining a Firewall Rule
By default, incoming traffic from the outside network is blocked. We will define a new firewall rule for RStudio Server (port 8787) and/or Jupyter Notebook (port 8888).

1. In the navigation panel and go to VPC network >  Firewall rules.
    <center><img src="resources/img/firewall_rules.png" width="300"></center>
1. To create a new firewall rule, select "Create Firewall Rule".
1. Fill in the form as such:
    <center><img src="resources/img/new_firewall_rule.png" height="500"></center>

    - Name: r-studio
    - Description: open port for r-studio
    - Target tags: r-studio
    - Source IP ranges: 0.0.0.0/0
    - Protocols and ports: tcp:8787

    <center><img src="resources/img/firewall-jupyter-notebook.png" height="500"></center>

    - Name: jupyter-notebook
    - Description: Jupyter Notebook on port 8888
    - Target tags: jupyter-notebook
    - Source IP ranges: 0.0.0.0/0
    - Protocols and ports: tcp:8888
1. Stop the VM so we can make network changes.
    <center><img src="resources/img/vm-stop.png" width="550"></center>
1. Click on the VM name to view VM instance details.
    <center><img src="resources/img/vm-vm-instance-details.png" width="550"></center>
1. Edit the VM instance details to include the firewall rule to expose port 8787.
    <center><img src="resources/img/vm-vm-instance-edit.png" width="550"></center>
1. Add the Network tag `r-studio` and `jupyter-notebook` to assign the firewall rule we just defined and save.
    <center><img src="resources/img/vm-vm-instance-network-tag.png" height="500"></center>
1. Start the VM and connect via SSH.

## Setting a Static IP
By default, the IP assigned to the VM is ephemeral meaning that it may change at any time. This means that if we have an RStudio Server or Jupyter Notebook running, the IP address may change at any time and our work will not persist. To avoid this, we can set a static IP by navigating to 'VPC Network' > 'External IP addresses'.

<center><img src="resources/img/vm-external-ip.png" width="300"></center>

Then, change the IP type from 'Epemeral' to 'Static' for the appropriate VM.

<center><img src="resources/img/vm-external-ip-static.png" width="600"></center>
