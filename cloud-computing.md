# Cloud Computing

Data analysis is a rapidly evolving process. Data used to be shared via physical disks and analyzed using proprietary software. Today, big data can't be saved and analyzed on a personal computer due to the sheer size. Instead, we use servers that are capable of handling big data that can be personally maintained or managed by the cloud provider. To analyze, we turn to open source software tools such as data.table in R and pandas in Python.

We are now collecting data from almost every domain. Analysis will be ever more collaborative due to size and complexity. We will need a mechanism to collaborate, a way to obtain and track software tools, and an integrated place for interdisciplinary teams.

We will be leveraging:
- the cloud (specifically GCP) to store our big data
- Git to version control our code files and documentation
- Anaconda to track the external open source software tools we use

By using these tools, we no longer need to worry about scaling our team of 2 to a team of 200. Any increase in team size will not hinder our team's productivity.

## What is a "cloud"?
- network of computing equipment
<center><img src="resources/img/wiki_cloud_computing.png" width="500"></center>


Underwater Data Center?
<center><img src="resources/img/MSFT_Natick_Deployment.jpg" width="500"></center>

## Reasons for shifting to the cloud:
- "Serverless" scaling
    - Can get a big or small machine at anytime and only pay for as much as you need (vs fixed cost).
- Minimum management.
    - No need for hardware security updates - the world's best security engineers are providing security patches.
- Availability and reliability.
    - No longer need to wait for business hours to resolve a creation of a VM. You can do it yourself!
    - Each cloud provider has a service level agreement (SLA) - often greater than 99.99% uptime.

## Reasons against shifting to the cloud:
- Legal risks.
    - Compliant with the laws of the country where the server is located?
        - When working with healthcare data, is this HIPAA compliant? HITRUST CSF certified? Some cloud providers with obtain the certification for you while others place the responsibility on the user.
- Requires a good internet connection to use cloud services.
    - However, can still do some work locally if preferred.
- On going costs.
    - If not careful, may end up with a large bill. Remember to shut down
    services that aren't being used and explore ways to decrease cost by choosing
    a different service within the cloud.

## Cloud Providers
- [Amazon Web Services](https://aws.amazon.com/)
    - [AWS Machine Learning Research Awards](https://aws.amazon.com/aws-ml-research-awards/)
- [Google Cloud Platform](https://cloud.google.com/)
    - [GCP: Education Credits](https://cloud.google.com/edu/?options=research-credits#options)
- [Microsoft Azure](https://azure.microsoft.com/)
    - [Microsoft Azure for Research](https://www.microsoft.com/en-us/research/academic-program/microsoft-azure-for-research/)
    - [Azure for Research Award: Data Science](https://www.microsoft.com/en-us/research/academic-program/data-science-award/)

We will be exploring the Google Cloud Platform. If you
are more familiar with Amazon Web Services, take a look
at the [quick reference mapping of AWS products](https://cloud.google.com/docs/compare/aws/).
